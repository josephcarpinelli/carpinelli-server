#!/usr/bin/env python3

"""Flask introduction app. Task manager server tutorial."""

"""Author: Joseph Carpinelli
   Last updated: 2021-01-15"""

from datetime import datetime
import logging
from pathlib import Path
import traceback

from flask import Flask
from flask import render_template
from flask import url_for
from flask import request
from flask import redirect
from flask_sqlalchemy import SQLAlchemy


"""Put "/grocery-list/" in a str variable"""
"""Lookup jinju syntax"""
"""Use pip freeze > requirements.txt"""
"""Use git init"""


app = Flask(__name__)
# In "sqlite:///", 3 slashes is relative, 4 is absolute
db_name = "sqlite:///grocery_list.db"
app.config["SQLALCHEMY_DATABASE_URI"] = db_name

db = SQLAlchemy(app)
# Check if database exists, if not, create it.
# This has to be done in an interactive python shell.
# python3; from app import db; db.create_all();
if not Path("./%s" % db_name).exists():
    db.create_all()


class ItemList(db.Model):
    """ItemList db.Model."""
    id = db.Column(db.Integer, primary_key=True)
    # nullable=False prevents user from entering a blank item
    content = db.Column(db.String(200), nullable=False)
    completed = db.Column(db.Integer, default=0)  # Not implemented
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self) -> str:
        """Return the task ID when creating a new  element."""
        return "<Task %r>" % self.id


@app.route("/")
def index() -> str:
    """Return the index page for the server."""
    return render_template("index.html")


@app.route("/grocery-list/", methods=["POST", "GET"])
def grocery_list() -> str:
    """Return a task manager page.
    This page is located at '/task-manager/'.
    THIS IT OUTDOATED: 2021-01-13"""
    if request.method == "POST":
        item_content = request.form["content"]  # ID of the input we want the contents of.
        new_item = ItemList(content=item_content)

        try:
            db.session.add(new_item)
            db.session.commit()
            return redirect("/grocery-list/")
        except:
            return "There was an issue adding to the list."
    else:
        # Can also use '.first()' instead of '.all()'
        grocery_items = ItemList.query.order_by(ItemList.date_created).all()
        return render_template("grocery-list.html", grocery_items=grocery_items)


@app.route("/grocery-list/remove/<int:id>")
def remove(id) -> str:
    """Remove a grocery item."""
    item_to_delete = ItemList.query.get_or_404(id)

    try:
        db.session.delete(item_to_delete)
        db.session.commit()
        return redirect("/grocery-list/")
    except:
        return "There was a problem deleting this task."


@app.route("/grocery-list/update/<int:id>", methods=["GET", "POST"])
def update(id) -> str:
    """Update a grocery item."""
    grocery_item = ItemList.query.get_or_404(id)

    if request.method == "POST":
        grocery_item.content = request.form["content"]
        
        try:
            db.session.commit()
            return redirect("/grocery-list/")
        except:
            return "There was a problem updating the grocery item."
    else:
        return render_template("item-update.html", grocery_item=grocery_item)

"""Plans for a tally list page. For example, fastfood."""


if __name__ == "__main__":
    logging.basicConfig(# filename="app.log",  #  Commented out to default to console.
                        #  mode='w')  # Write a clean log file for each run.
                        level=logging.DEBUG)  # Print all logging levels.
                        
    try:
        """
        app.run(debug=True, host="0.0.0.0")
        """
        app.run(host="0.0.0.0")
    except:
        logging.critical(traceback.format_exc())

